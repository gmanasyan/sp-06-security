package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Task;

import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface ITaskService {

    Task createTask(String name);

    Task getTaskById(String id);

    Task merge(Task task);

    boolean existById(final String id);

    void removeTaskById(String id);

    List<Task> getListTask();

    void clear();

    Task createTaskByProject(String projectName, String taskName);

    Task getByName(String taskName);

    void merge(Task... tasks);

    void merge(Collection<Task> tasks);

    void removeTaskByOrderIndex(String taskName);

}
