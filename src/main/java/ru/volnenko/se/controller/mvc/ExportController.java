package ru.volnenko.se.controller.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.volnenko.se.api.service.IDataService;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Domain;
import ru.volnenko.se.entity.Project;

@Controller
public class ExportController {

    @Autowired
    private IDataService dataService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @GetMapping("/export-json")
    @ResponseBody
    public String exportJson() {
        Domain domain = new Domain();
        domain.setProjects(projectService.getListProject());
        domain.setTasks(taskService.getListTask());
        return dataService.exportJson(domain);
    }
}
