package ru.volnenko.se.controller.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @RequestMapping("/mvc")
    public String mvc() {
        return "view";
    }

    @RequestMapping({"/","/projects"})
    public ModelAndView model() {
        List<Project> projects = projectService.getListProject();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @GetMapping("/add-project")
    public String addProject() {
        return "addproject";
    }

    @GetMapping("/view-project")
    public ModelAndView viewProject(@RequestParam("projectId") String projectId ) {
        Project project = projectService.getProjectById(projectId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("viewproject");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @GetMapping("/remove-project")
    public String addProject(@RequestParam("projectId") String projectId ) {
        projectService.removeProjectById(projectId);
        return "redirect:/";
    }

    @PostMapping("/add-project")
    public String addProject(Project project) {
        projectService.merge(project);
        return "redirect:/";
    }

    @GetMapping("/edit-project")
    public ModelAndView editProject(@RequestParam("projectId") String projectId ) {
        Project project = projectService.getProjectById(projectId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editproject");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @PostMapping("/edit-project")
    public String editProject(Project project) {
        projectService.merge(project);
        return "redirect:/";
    }
}
