package ru.volnenko.se.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Task;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestTaskController {

    @Autowired
    private ITaskService taskService;

    @GetMapping("/")
    public List<Task> getTasks() {
        return taskService.getListTask();
    }

    @GetMapping("/{id}")
    public Task getTask(@PathVariable("id") String id) {
        return taskService.getTaskById(id);
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Task addTask(@RequestBody Task task) {
        return taskService.merge(task);
    }

    @PutMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editTask(@RequestBody Task task) {
        return taskService.existById(task.getId()) ?
                ResponseEntity.ok(taskService.merge(task)) : ResponseEntity.badRequest().build();
    }

    @DeleteMapping(value = "/{id}")
    public void deleteTask(@PathVariable String id) {
        try {
            taskService.removeTaskById(id);
        } catch (Exception e) {
            System.out.println("Log: attempt to delete non existing task");
        }
    }

}
