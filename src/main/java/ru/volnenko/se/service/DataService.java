package ru.volnenko.se.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.service.IDataService;
import ru.volnenko.se.entity.Domain;

import java.io.*;

@Service
public class DataService implements IDataService {

    @Override
    public String exportJson(Domain data) {
        final ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void saveJson(Domain data) {
        final ObjectMapper mapper = new ObjectMapper();
        try (Writer writer = new FileWriter("data.json")) {
            String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
            System.out.println(result);
            writer.append(result);
        } catch (Exception e) {
            System.out.println("Ошибка записи json: " + e.getMessage());
        }
    }

    @Override
    public Domain loadJson() {

        final ObjectMapper mapper = new ObjectMapper();
        Domain domain = null;
        try {
            domain = mapper.readValue(new File("data.json"), Domain.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return domain;
    }

    @Override
    public void saveXml(Domain data) {
        final ObjectMapper mapper = new XmlMapper();
        try (Writer writer = new FileWriter("data.xml")) {
            String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
            System.out.println(result);
            writer.append(result);
        } catch (Exception e) {
            System.out.println("Ошибка записи xml: " + e.getMessage());
        }
    }

    @Override
    public Domain loadXml() {
        final ObjectMapper mapper = new XmlMapper();
        Domain domain = null;
        try {
            domain = mapper.readValue(new File("data.xml"), Domain.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return domain;
    }
}
