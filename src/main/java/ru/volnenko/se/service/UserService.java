package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.entity.Role;
import ru.volnenko.se.entity.RoleType;
import ru.volnenko.se.entity.User;
import ru.volnenko.se.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("user", "user", RoleType.MANAGER);
    }

    private void initUser(final String login, final String password, final RoleType roleType) {
        final User user = userRepository.findByLogin(login);
        if (user != null) return;
        if (login == null || password == null ||
                login.isEmpty() || password.isEmpty()) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(final String login, final String password, final RoleType roleType) {
        final User user = new User();
        user.setLogin(login);
        user.setPassword(passwordEncoder.encode(password));

        final Role role = new Role();
        role.setRoleType(roleType);;
        role.setUser(user);

        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}
