<jsp:include page="parts/header.jsp"/>

<div class="box">

<h2>Add Task</h2>

    <form method="post" action="add-task">
        <dl>
            <dt>Project:</dt>
            <dd>
                <select name="project.id">
                    <option value=""></option>
                    <c:forEach items="${projects}" var="project">
                    <jsp:useBean id="project" type="ru.volnenko.se.entity.Project"/>
                        <option value="${project.id}">${project.name}</option>
                    </c:forEach>
                </select>
            </dd>
        </dl>
        <dl>
            <dt>Task name:</dt>
            <dd><input type="text" value="" size=40 name="name" required></dd>
        </dl>
        <dl>
            <dt>Description:</dt>
            <dd><input type="text" value="" size=40 name="description" required></dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><input type="date" name="dateBegin" required></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><input type="date" name="dateEnd" required></dd>
        </dl>
        <button type="submit">Save</button>
        <button onclick="window.history.back()" type="button">Cancel</button>
    </form>

</div>

<jsp:include page="parts/footer.jsp"/>