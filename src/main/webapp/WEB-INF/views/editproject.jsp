<jsp:include page="parts/header.jsp"/>

<div class="box">

<h2>Edit Project</h2>

    <form method="post" action="edit-project">
        <dl>
            <dd><input hidden type="text" value="${project.id}" size=40 name="id" required></dd>
        </dl>
        <dl>
            <dt>Name of project:</dt>
            <dd><input type="text" value="${project.name}" size=40 name="name" required></dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><input type="date" value="<fmt:formatDate value="${project.dateBegin}" pattern="yyyy-MM-dd"/>" name="dateBegin" required></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><input type="date" value="<fmt:formatDate value="${project.dateEnd}" pattern="yyyy-MM-dd"/>" name="dateEnd" required></dd>
        </dl>
        <button type="submit">Save</button>
        <button onclick="window.history.back()" type="button">Cancel</button>
    </form>

</div>

<jsp:include page="parts/footer.jsp"/>