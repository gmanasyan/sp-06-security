<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Main Page</title>
    <link rel="stylesheet" href="assets/css/style.css">
<body>

<div class="header">
    <a href="projects">Projects</a>
    <a href="tasks">Tasks</a>
    <sec:authorize access="hasRole('ADMIN')">
        <a href="export-json">Export Json</a>
    </sec:authorize>
    <div style="float: right; color: white">
      <sec:authorize access="isAuthenticated()">
          Hello <sec:authentication property="name"/>
          <a href="logout">Logout</a>
      </sec:authorize>
    </div>
</div>
