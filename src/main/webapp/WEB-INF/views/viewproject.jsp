<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<jsp:include page="parts/header.jsp"/>

<div class="box">

<h2>View Project</h2>

    <form method="post" action="edit-project">
        <dl>
            <dt>Project ID:</dt>
            <dd>${project.id}</dd>
        </dl>
        <dl>
            <dt>Name of project:</dt>
            <dd>${project.name}</dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><fmt:formatDate value="${project.dateBegin}" pattern="yyyy-MM-dd"/></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><fmt:formatDate value="${project.dateEnd}" pattern="yyyy-MM-dd"/></dd>
        </dl>
    </form>
    <a class="button" onclick="window.history.back()">Back</a>
</div>

<jsp:include page="parts/footer.jsp"/>